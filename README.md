# rtpck

Package  provides the capabilities to pick the closest destination from a given source based on the driving time and distance.
It uses OSRM router service to determine each individual route, groups them together, and represents to the user in JSON format, sorted by
proximity

### Installing

Building manually:

```
go install .

go test ./...
```

Using docker:

```
docker build -t rtpck .
```

## Usage

```
rtpck
```

by default service starts at `localhost:8080` use:

```
rtpck -h 
```

for instructions how to change port.

Using docker:

```
docker run -p "8080:8080" rtpck
```
