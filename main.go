package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"gitlab.com/pBiczysko/rtpck/rtpck"
)

const osrmRouterURL = "http://router.project-osrm.org/route/v1/driving/"

func main() {

	port := flag.String("p", "8080", "specify the binding port")
	flag.Parse()

	rtr := rtpck.New(osrmRouterURL, rtpck.WithMaxAttempt(4), rtpck.WithMaxWaitTime(5*time.Second))
	http.Handle("/routes", rtr)

	log.Println("Listening on port", *port)
	http.ListenAndServe(":"+*port, nil)
}
