FROM golang:1.10.2 as builder

WORKDIR /go/src/gitlab.com/pBiczysko/rtpck

ADD . .

RUN go get -d -v

RUN go test ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/pBiczysko/rtpck/app .

ENTRYPOINT ["./app"]