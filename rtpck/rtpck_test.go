package rtpck_test

import (
	"sort"
	"testing"

	"gitlab.com/pBiczysko/rtpck/rtpck"
)

func TestRouteSorting(t *testing.T) {

	rt_1 := rtpck.Route{
		Destination: "dst_1",
		Duration:    10,
		Distance:    15,
	}
	rt_2 := rtpck.Route{
		Destination: "dst_2",
		Duration:    10,
		Distance:    16,
	}
	rt_3 := rtpck.Route{
		Destination: "dst_3",
		Duration:    8,
		Distance:    20,
	}
	rt_4 := rtpck.Route{
		Destination: "dst_4",
		Duration:    8,
		Distance:    20,
	}

	tests := []struct {
		name        string
		rts         rtpck.Routes
		expectedRts rtpck.Routes
	}{
		{"simple", rtpck.Routes{rt_1, rt_3}, rtpck.Routes{rt_3, rt_1}},
		{"same_duration", rtpck.Routes{rt_2, rt_1}, rtpck.Routes{rt_1, rt_2}},
		{"same_duration_same_time", rtpck.Routes{rt_3, rt_4}, rtpck.Routes{rt_3, rt_4}},
		{"complex", rtpck.Routes{rt_1, rt_2, rt_3, rt_4}, rtpck.Routes{rt_3, rt_4, rt_1, rt_2}},
	}

	for _, tt := range tests {
		tf := func(t *testing.T) {
			sort.Sort(tt.rts)
			if !testEq(tt.rts, tt.expectedRts) {
				t.Errorf("Resulting slices are not equal expected: %v, got %v", tt.rts, tt.expectedRts)
			}

		}
		t.Run(tt.name, tf)
	}
}

func testEq(a, b []rtpck.Route) bool {

	// If one is nil, the other must also be nil.
	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
