// Package rtpck provides the capabilities to pick the closest destination from a given source, based on the driving time and distance.
// It uses OSRM router service to determine each individual route, groups them together, and represents to the user in JSON format, sorted by
// proximity
package rtpck

import (
	"encoding/json"
	"errors"
	"log"
	"math"
	"net/http"
	"net/url"
	"path"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

const osrmOk = "Ok"

var (
	errTooManyRequests    = errors.New("Too many requests reported by the OSRM service")
	errInvalidSource      = errors.New("Provided source location is invalid")
	errInvalidDestination = errors.New("Provided destination location is invalid")
	errOSRMFail           = errors.New("OSRM service reported fail during routing")
)

var (
	// DefaultMaxAttempts - default value for max attempts to OSRM route service
	DefaultMaxAttempts = 5

	// DefaultMinWaitTime - default value for min wait time between attempts to OSRM service
	DefaultMinWaitTime = 1 * time.Second

	// DefaultMaxWaitTime - default value for max wait time between attempts to OSRM service
	DefaultMaxWaitTime = 10 * time.Second
)

// RoutePicker - represents a type used to get the routes from provided source to provided destinations
// Because OSRM experiences too many requests it often drops them.
// In order to make the rtpck service more reliable the basic retry mechanism was introduces with exponential backoff strategy
// By using Options the default retry parameters can be altered.
type RoutePicker struct {
	routerServiceURL string
	maxAttempts      int
	minWaitTime      time.Duration
	maxWaitTime      time.Duration
}

// New - constructs RoutePicker value based on the routerServiceURL and provided options
func New(rsu string, opts ...Option) *RoutePicker {
	rp := RoutePicker{
		routerServiceURL: rsu,
		maxAttempts:      DefaultMaxAttempts,
		minWaitTime:      DefaultMinWaitTime,
		maxWaitTime:      DefaultMaxWaitTime,
	}

	for _, opt := range opts {
		opt(&rp)
	}

	return &rp
}

// Option - represent type used to configure constructed RoutePicker
type Option func(*RoutePicker)

// WithMaxAttempt - option to define max attempts number done in communication with OSRM
func WithMaxAttempt(maxAtt int) Option {
	return func(rp *RoutePicker) {
		rp.maxAttempts = maxAtt
	}
}

// WithMinWaitTime - option to define min wait time between requests to OSRM
func WithMinWaitTime(minWT time.Duration) Option {
	return func(rp *RoutePicker) {
		rp.minWaitTime = minWT
	}
}

// WithMaxWaitTime - option to define max wait time between requests to OSRM
func WithMaxWaitTime(maxWT time.Duration) Option {
	return func(rp *RoutePicker) {
		rp.maxWaitTime = maxWT
	}
}

// Result - represents list of routes between the source and each of the destinations.
// That structure is returned to the user as JSON
type Result struct {
	Source string `json:"source"`
	Routes Routes `json:"routes"`
}

// Routes - slice of route values that enables to sort based on driving distance and
// duration from source to destination
type Routes []Route

// Route - represents a single route from source to specific destination.
// Contains information about distance needed to travel that route by car and
// how long it takes.
type Route struct {
	Destination string  `json:"destination"`
	Duration    float64 `json:"duration"`
	Distance    float64 `json:"distance"`
}

// routeResponse - represents a structure returned by OSRM service regarding
// a route from the source to a single destination
type routeResponse struct {
	Routes []struct {
		Duration float64 `json:"duration"`
		Distance float64 `json:"distance"`
	} `json:"routes"`
	Code string `json:"code"`
}

// ServeHTTP - RoutePicker implements http.ServeHTTP interface
func (rp *RoutePicker) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	src := r.URL.Query().Get("src")
	dsts := r.URL.Query()["dst"]

	res, err := rp.GetRoutes(src, dsts...)
	if err != nil {
		switch err {
		case errTooManyRequests:
			http.Error(w, errTooManyRequests.Error(), http.StatusInternalServerError)
		case errInvalidSource:
			http.Error(w, errInvalidSource.Error(), http.StatusBadRequest)
		case errInvalidDestination:
			http.Error(w, errInvalidDestination.Error(), http.StatusBadRequest)
		case errOSRMFail:
			http.Error(w, errOSRMFail.Error(), http.StatusInternalServerError)
		default:
			log.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return
	}

	output, err := json.MarshalIndent(res, "", "  ")
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
}

// GetRoutes - used to get the routes from provided source to each of the specified destinations
// sorted by driving time and distance (if time is equal).
// It first performs the validation of the query parameters provided by the user
// than makes API call to rhe Router service for each of the destination and constructs the result.
// It uses simple retry mechanism based on exponential backoff and parameters specified in RoutePicker
func (rp *RoutePicker) GetRoutes(src string, dsts ...string) (Result, error) {
	if err := validateQuery(src, dsts...); err != nil {
		return Result{}, err
	}
	// create error channel to communicate errors during getRoute
	errs := make(chan error, 1)
	routes := make(chan Route, len(dsts))

	var wg sync.WaitGroup
	wg.Add(len(dsts))

	// create a worker function that given the destination would
	// get the corresponding route or report an error
	wrk := func(dst string) {
		defer wg.Done()
		rt, err := rp.getRoute(src, dst)
		if err != nil {
			select {
			case errs <- err:
				// send error into the channel
			default:
				// there is already error on the channel -> return
				return
			}
		}
		routes <- rt
	}

	for _, dst := range dsts {
		go wrk(dst)
	}

	go func() {
		wg.Wait()
		close(errs)
		close(routes)
	}()

	if err := <-errs; err != nil {
		return Result{}, err
	}

	var rts Routes
	for rt := range routes {
		rts = append(rts, rt)
	}

	res := createResults(rts, src)
	return res, nil
}

// getRoute - is used to get a route from source to single destination
func (rp *RoutePicker) getRoute(src, dst string) (Route, error) {
	query, err := rp.buildURLString(src, dst)
	if err != nil {
		return Route{}, err
	}

	var resp *http.Response

	for i := 0; i < rp.maxAttempts; i++ {

		resp, err = http.Get(query)
		if err != nil {
			return Route{}, err
		}
		defer resp.Body.Close()

		if resp.StatusCode == http.StatusTooManyRequests {
			if i < rp.maxAttempts-1 {
				backoff(rp.minWaitTime, rp.maxWaitTime, i)
			}
		}
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusTooManyRequests {
		return Route{}, errTooManyRequests
	}

	var rtr routeResponse
	err = json.NewDecoder(resp.Body).Decode(&rtr)
	if err != nil {
		return Route{}, err
	}

	if rtr.Code != osrmOk {
		return Route{}, errOSRMFail
	}

	rt := Route{
		Destination: dst,
		Duration:    rtr.Routes[0].Duration,
		Distance:    rtr.Routes[0].Distance,
	}

	return rt, nil
}

// buildURLString - using the parameters provided by the user in the query string
// and URL of the router service it creates a complete URL string to call router service with
func (rp *RoutePicker) buildURLString(src, dst string) (string, error) {
	q, err := url.Parse(rp.routerServiceURL)
	if err != nil {
		return "", err
	}

	coords := strings.Join([]string{src, dst}, ";")
	q.Path = path.Join(q.Path, coords)
	qs := q.Query()
	qs.Add("overview", "false")
	q.RawQuery = qs.Encode()

	return q.String(), nil
}

// createResults - sorts provided routes and creates a Result value
// to be encoded as JSON and returned to user.
func createResults(rts Routes, src string) Result {
	var res Result
	sort.Sort(rts)
	res.Routes = rts
	res.Source = src
	return res
}

// validateQuery - checks whether user provided URL query parameters
// are a valid pair of latitude and longitude
func validateQuery(src string, dsts ...string) error {
	if src == "" || !isValidCoordinate(src) {
		return errInvalidSource
	}

	for _, dst := range dsts {
		if dst == "" || !isValidCoordinate(dst) {
			return errInvalidDestination
		}
	}

	return nil
}

// isValidCoordinate - checks whether provided string
// represents a a valid pair of latitude and longitude.
// checks whether are both numerical values.
func isValidCoordinate(coord string) bool {
	if !strings.Contains(coord, ",") {
		return false
	}

	longLatStr := strings.Split(coord, ",")
	longStr := longLatStr[0]
	latStr := longLatStr[1]

	if _, err := strconv.ParseFloat(longStr, 64); err != nil {
		return false
	}

	if _, err := strconv.ParseFloat(latStr, 64); err != nil {
		return false
	}

	return true
}

// Less - order routes by duration and if equal use the distance
// Routes implements sort.Interface
func (rts Routes) Less(i, j int) bool {
	if rts[i].Duration != rts[j].Duration {
		return rts[i].Duration < rts[j].Duration
	}
	return rts[i].Distance < rts[j].Distance
}

// Len - return length of the routes slice
// Routes implements sort.Interface
func (rts Routes) Len() int {
	return len(rts)
}

// Swap - swap two routes in the slice
// Routes implements sort.Interface
func (rts Routes) Swap(i, j int) {
	rts[i], rts[j] = rts[j], rts[i]
}

// backoff - used to determine and execute a wait period
// between requests to OSRM
func backoff(min, max time.Duration, attempt int) {
	mult := math.Pow(2, float64(attempt)) * float64(min)
	sleep := time.Duration(mult)
	if float64(sleep) != mult || sleep > max {
		sleep = max
	}
	time.Sleep(sleep)
	return
}
